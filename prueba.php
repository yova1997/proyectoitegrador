<!DOCTYPE HTML>
<!--
	Landed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Sweet Solution</title>
		<meta charset="utf-8" />
		<link rel="icon" type="image/png" href="../resources/icon/rosquilla.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../css/main.css" />
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<noscript><link rel="stylesheet" href="../css/noscript.css" /></noscript>
	</head>
	<body class="is-preload landing">
			<section id="two1" class="content">
				<p>
				<h2>HISTORIA</h2>
				
				Todo comenzó con el gran deseo y amor de la repostería por medio de la abuela
				Mari. Ella solía hornear un postre, el favorito de la familia el cual se comían en
				menos de lo que dura un dulce sabor agradable al paladar, y de ahí es que nace el
				actual nombre que nos caracteriza: Cherry Solutions.
				<br></br>
				Desde el año 2017, la historia de Cherry Solutions se ha comenzado a hornear.
				Cada colaborador se ha convertido en un ingrediente de calidad que en conjunto
				han dado como resultado el mejor pastel de Chiapas. Comenzando como una idea
				de negocio familiar, donde la crisis afectaba a todos y una visualización que existía
				<br></br>
				en la familia, hicieron que un gusto por la repostería se convirtiera en lo que hoy
				es Cherry Solutions.
				Tuxtla Gutiérrez fue la cuna, abriéndose la primera sucursal en la Colonia 24 de
				junio, logrando un sueño que se fortalece diariamente con el trabajo de todos los
				colaboradores.
				<br></br>
				Somos la pastelería más grande en Chiapas, con presencial nacional, un orgullo
				Chiapaneco que deleita el paladar de miles de familias creando una experiencia
				de delicias inigualable.
				Como parte de la expansión de Cherry Solutions, en el año 2018 se hace una
				apuesta para incluir Franquicias en nuestro modelo de negocio. De esta manera,
				se abre Delicia Única en San Cristóbal de las casas para fortalecer la marca. Hoy
				en día, son gran parte del éxito de nuestra empresa. Es así como hemos creado la
				mejor pastelería y donde a partir de tu ingreso, comienzas a escribir una hoja de
				nuestro recetario que incluye amigos, responsabilidades, conocimientos,
				habilidades y mucha pasión por lo que hacemos.
				</p>
				<center><span class="styleimage"><img src="../resources/images/stickermini.jpg" alt=""/></span></center>
				<ul class="actions">
					<li><a href="index.php" class="button">Back</a></li>
				</ul>
				
				
			</section>

		<!-- Scripts -->
			<script src="../js/jquery.min.js"></script>
			<script src="../js/jquery.scrolly.min.js"></script>
			<script src="../js/jquery.dropotron.min.js"></script>
			<script src="../js/jquery.scrollex.min.js"></script>
			<script src="../js/browser.min.js"></script>
			<script src="../js/breakpoints.min.js"></script>
			<script src="../js/util.js"></script>
			<script src="../js/main.js"></script>

	</body>
</html>