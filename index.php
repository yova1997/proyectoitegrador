<?php include_once("consultas.php"); ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>CherrySolutions</title>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="resources/icon/rosquilla.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="css/main.css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<noscript><link rel="stylesheet" href="css/noscript.css" /></noscript>
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<header id="header">
					<h1 id="logo"><a href="index.html">CherrySolutions</a></h1>
						<nav id="nav">
						<ul>
							<li>
								<a href="#">Categoria</a>
								<ul>
								<?php $query = "select * from categorias";
									  $resultadoCategorias = $mysqli->query($query);
									  if ($resultadoCategorias->num_rows > 0)
									  {
									  while ($row = $resultadoCategorias->fetch_assoc()) 
									  {?>
										  <li><a href="<?php echo "categoria/$row[nombre_archivo].php"; ?>"><?php echo "$row[nombre]"; ?></a></li>

									<?php  }  	
									  
									  }	 ?>
									
									
								</ul>
							</li>
							<li><a href="carrito.php">Carrito</a></li>
							<li><a href="Register.php" target="_self" class="button primary">Registrarse</a></li>
							<li><a href="login.php" target="_self" class="button primary">Iniciar sesión</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<div class="content">
						<header>
							<h2>CherrySolutions</h2>
						</header>
						<span class="image"><img src="resources/images/ImagenInicio.jpg" alt="" /></span>
					</div>
					<a href="#one" class="goto-next scrolly">Siguiente</a>
				</section>

			<!-- One -->
				<section id="one" class="spotlight style1 bottom">
					<span class="image fit main"><img src="resources/images/ImagenInicio.jpg" alt="" /></span>
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="col-4 col-12-medium">
									<header>
										<?php
											include_once("consultas.php");
											mysqli_set_charset($mysqli, "utf8");  //formato sin karacteres feos
											$query = "select nombre_info from info_general where nombre_info = 'Visión'";
											$resultado = $mysqli->query($query);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[nombre_info]";
											}  
											
											}
										?>
										<br></br>
										<?php
											$query1 = "select descripcion_info from info_general where id_info = 5";
											$resultado = $mysqli->query($query1);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[descripcion_info]";
											}  
											
											}
										?>
									</header>
								</div>
								<div class="col-4 col-12-medium">
									<?php
											include_once("consultas.php");
											mysqli_set_charset($mysqli, "utf8");  //formato sin karacteres feos
											$query = "select nombre_info from info_general where nombre_info = 'Misión'";
											$resultado = $mysqli->query($query);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[nombre_info]";
											}  
											
											}
										?>
										<br></br>
										<?php
											$query1 = "select descripcion_info from info_general where id_info = 4";
											$resultado = $mysqli->query($query1);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[descripcion_info]";
											}  
											
											}
										?>
								</div>
								<div class="col-4 col-12-medium">
									<?php
											include_once("consultas.php");
											mysqli_set_charset($mysqli, "utf8");  //formato sin karacteres feos
											$query = "select nombre_info from info_general where nombre_info = 'Valores'";
											$resultado = $mysqli->query($query);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												
												echo "$row[nombre_info]";
											}  
											
											}
										?>
										<br></br>
										<?php
											$query1 = "select descripcion_info from info_general where id_info = 6";
											$resultado = $mysqli->query($query1);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[descripcion_info]";
											}  
											
											}
										?>
								</div>
							</div>
						</div>
					</div>
					<a href="#two" class="goto-next scrolly">Siguiente</a>
				</section>

			<!-- Two -->
				<section id="two" class="spotlight style2 right">
					<span class="image fit main"><img src="resources/images/varios.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>HISTORIA</h2>
						</header>
						<p>
							Todo comenzó con el gran deseo y amor de la repostería por medio de la abuela
							Mari. Ella solía hornear un postre, el favorito de la familia el cual se comían en
							menos de lo que dura un dulce sabor agradable al paladar, y de ahí es que nace el
							actual nombre que nos caracteriza: Cherry Solutions.
							
							Desde el año 2017, la historia de Sweet Solutions se ha comenzado a hornear.
							Cada colaborador se ha convertido en un ingrediente de calidad que en conjunto
							han dado como resultado el mejor pastel de Chiapas.
						</p>
						<ul class="actions">
							<li><a href="prueba.php" class="button">leer mas</a></li>
						</ul>
					</div>
					<a href="#three" class="goto-next scrolly">Siguiente</a>
					</section>
					

			<!-- Five -->
				<section id="five" class="wrapper style2 special fade">
					<div class="container">
						<header>
							<h2>¡¡¡¡¡...VISITANOS......NO TE ARREPENTIRAS...!!!!!</h2>
							<?php
											include_once("consultas.php");
											mysqli_set_charset($mysqli, "utf8");  //formato sin karacteres feos
											$query = "select nombre_info from info_general where nombre_info = 'Ubicación'";
											$resultado = $mysqli->query($query);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[nombre_info]";
											}  
											
											}
										?>
										<br></br>
										<?php
											$query1 = "select descripcion_info from info_general where id_info = 7";
											$resultado = $mysqli->query($query1);
											if ($resultado->num_rows > 0)
											{
											while ($row = $resultado->fetch_assoc()) 
											{
												echo "$row[descripcion_info]";
											}  
											
											}
										?>
						</header>
						
					</div>
				</section>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-facebook"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: cherry</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="js/jquery.min.js"></script>
			<script src="js/jquery.scrolly.min.js"></script>
			<script src="js/jquery.dropotron.min.js"></script>
			<script src="js/jquery.scrollex.min.js"></script>
			<script src="js/browser.min.js"></script>
			<script src="js/breakpoints.min.js"></script>
			<script src="js/util.js"></script>
			<script src="js/main.js"></script>

	</body>
</html>