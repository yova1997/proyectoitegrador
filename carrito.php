<?php include_once("./consultas.php");?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="resources/icon/rosquilla.png" />
	<title>Cherry Solution</title>
	<link rel="stylesheet" href="css/jquery.mobile-1.4.5.min.css" />
		<script src="js/jquery-1.11.1.min.js"></script>

		<link rel="stylesheet" href="css/jqm-demos.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
		<script src="js/jquery.js"></script>
		<script src="js/index.js"></script>
		<script src="js/jquery.mobile-1.4.5.min.js"></script>
</head>
<body>
<?php 
		
		if (isset($_GET['endBuy']) == 'endBuy') {
			if (isset($_SESSION['login']) && $_SESSION['login'] != false) {
				echo '<script type="text/javascript">alert("hay session")</script>'; 
				header("location: panel.php");
			}else{
				echo '<script type="text/javascript">alert("no hay session")</script>'; 
				include_once("login.php");
			}
		}
    	$sql = "SELECT * FROM compras";
		$resultadoCompras = $mysqli->query($sql);

		
	?> 
<div data-role="page" class="jqm-demos" style="background-color: #FCE9E5" data-quicklinks="true">

<div data-role="header" >
        <nav data-role="navbar">
            <ul> 
            	<li><a href="index.php" data-icon="home" target="_self" data-theme="b">Inicio</a></li>
                <li><a href="#" data-icon="user" data-theme="b">Perfil</a></li>
                <li><a href="#" data-icon="check" data-theme="b"><br></a></li>
                <li><a href="../carrito.php" data-icon="shop" data-theme="b">carrito</a></li>           
               </ul>
       </nav>
</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

	<div class="divContainerProduc">
		<table style="width: 100%" data-role="table"  id="table-column-toggle"  class="ui-responsive table-stroke">
	     <thead>
	       <tr>
			<th data-priority="2"></th>
	         <th>Nombre</th>
	         <th data-priority="3">Cantidad</th>
	         <th data-priority="5">Precio</th>
	       </tr>
	     </thead>
	     <tbody>
		<?php
		$totalCompra = 0; 
		 while ( $row = $resultadoCompras ->fetch_assoc()) {
			$totalCompra += $row['precio_unitario'];
			?>
	       <tr>
			<td style="width: 30%"><img src="<?php echo $row['url_imagen'] ?>" style="height:40%; width: 40% "></td>
	         <td><?php echo $row['nombre_producto'] ?></td>
	         <td><?php echo $row['cantidad_compra'] ?></td>
	         <td><?php echo $row['precio_unitario'] ?></td>
            <td><a href="consultas.php?id_producDelete=<?php echo $row['id_compra'] ?>" target="_self" class="ui-btn" value="Eliminar" style="background-color: red;width: 30%;">Eliminar</a></td>
	       </tr>
		<?php } ?>
	     </tbody>
			<tr>
				<td>Total de la compra: $<?php echo $totalCompra; ?></td>
			</tr>
			<tr>
				<td>
				<a class="ui-btn" style="width: 45%;">Cancelar compra</a>
				<a href="carrito.php?endBuy=endBuy" target="_self" class="ui-btn" style="width: 45%;">Terminar compra</a>
				</td>
			</tr>
	   </table>
	</div>

	</div>
</div><!-- /page -->
</body>
</html>
